/*
 * Copyright (c) 2015 LingoChamp Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.liulishuo.magicprogresswidget;

import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.SweepShader;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

/**
 * Created by Jacksgong on 12/8/15.
 */
public class MagicProgressCircle extends Component implements ISmoothTarget, Component.DrawTask, Component.EstimateSizeListener {

    private static final String MPC_PERCENT = "mpc_percent";
    private static final String MPC_STROKE_WIDTH = "mpc_stroke_width";
    private static final String MPC_START_COLOR = "mpc_start_color";
    private static final String MPC_END_COLOR = "mpc_end_color";
    private static final String MPC_DEFAULT_COLOR = "mpc_default_color";
    private static final String MPC_FOOT_OVER_HEAD = "mpc_foot_over_head";

    private int startColor = 0xFF00FFED;
    private int endColor = 0xFFED00FF;
    private int defaultColor = 0x1AFFFFFF;
    private int percentEndColor;
    private int strokeWidth;
    private float percent;
    private float lastPercent;
    private MyValueAnimator animatorValue;
    private long maxTime = 4000;

    // 用于渐变
    private Paint paint;
    private boolean isFootOverHead;
    private Paint startPaint;
    private Paint endPaint;
    private final RectFloat rectF = new RectFloat();
    private Color[] customColors;
    private Color[] fullColors;
    private Color[] emptyColors;
    private float[] customPositions;
    private float[] extremePositions;

    public MagicProgressCircle(Context context) {
        super(context);
        init(context, null);
    }

    public MagicProgressCircle(Context context, AttrSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public MagicProgressCircle(Context context, AttrSet attrs, String styleName) {
        super(context, attrs, styleName);
        init(context, attrs);
    }

    public MagicProgressCircle(Context context, AttrSet attrs, int resId) {
        super(context, attrs, resId);
        init(context, attrs);
    }

    private void init(final Context context, final AttrSet attrs) {
        float defaultPercent = 0;
        final int strokeWdithDefaultValue = (int) (vpToPx(getContext(), 18) + 0.5f);
        if (context == null || attrs == null) {
            percent = defaultPercent;
            strokeWidth = strokeWdithDefaultValue;
        } else {
            AttrUtil attrUtil = new AttrUtil(attrs);
            percent = attrUtil.getFloatValue(MPC_PERCENT, defaultPercent);
            strokeWidth = attrUtil.getDimensionValue(MPC_STROKE_WIDTH, strokeWdithDefaultValue);
            startColor = attrUtil.getColorValue(MPC_START_COLOR, startColor);
            endColor = attrUtil.getColorValue(MPC_END_COLOR, endColor);
            defaultColor = attrUtil.getColorValue(MPC_DEFAULT_COLOR, defaultColor);
            isFootOverHead = attrUtil.getBooleanValue(MPC_FOOT_OVER_HEAD, false);
        }

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStrokeWidth(strokeWidth);
        paint.setStyle(Paint.Style.STROKE_STYLE);
        paint.setStrokeJoin(Paint.Join.ROUND_JOIN);
        paint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);

        startPaint = new Paint();
        startPaint.setColor(new Color(startColor));
        startPaint.setAntiAlias(true);
        startPaint.setStyle(Paint.Style.FILL_STYLE);

        endPaint = new Paint();
        endPaint.setAntiAlias(true);
        endPaint.setStyle(Paint.Style.FILL_STYLE);

        refreshDelta();

        customColors = new Color[]{new Color(startColor), new Color(percentEndColor), new Color(defaultColor), new Color(defaultColor)};
        fullColors = new Color[]{new Color(startColor), new Color(endColor)};
        emptyColors = new Color[]{new Color(defaultColor), new Color(defaultColor)};

        customPositions = new float[4];
        customPositions[0] = 0;
        customPositions[3] = 1;

        extremePositions = new float[]{0, 1};

        addDrawTask(this);
        setEstimateSizeListener(this);
    }

    private void refreshDelta() {
        int endR = (endColor & 0xFF0000) >> 16;
        int endG = (endColor & 0xFF00) >> 8;
        int endB = (endColor & 0xFF);

        this.startR = (startColor & 0xFF0000) >> 16;
        this.startG = (startColor & 0xFF00) >> 8;
        this.startB = (startColor & 0xFF);

        deltaR = endR - startR;
        deltaG = endG - startG;
        deltaB = endB - startB;
    }

    @Override
    public float getPercent() {
        return this.percent;
    }

    @Override
    public void setPercent(float percent) {
        percent = percentIn01(percent);
        anim(0, percent, (long) (percent * maxTime));
    }

    @Override
    public void setPercent(float percent, long durationMillis) {
        percent = percentIn01(percent);
        anim(0, percent, durationMillis);
    }

    @Override
    public void setSmoothPercent(float percent) {
        percent = percentIn01(percent);
        anim(lastPercent, percent, (long) ((percent - lastPercent) * maxTime));
    }

    @Override
    public void setSmoothPercent(float percent, long durationMillis) {
        percent = percentIn01(percent);
        anim(lastPercent, percent, durationMillis);
    }

    private void anim(float lastPercent, float percent, long duration) {
        if (animatorValue != null) {
            if (animatorValue.isRunning()) {
                animatorValue.end();
            }
            animatorValue.release();
        }
        animatorValue = MyValueAnimator.ofFloat(lastPercent, percent);
        animatorValue.setDuration(duration);
        animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float value) {
                MagicProgressCircle.this.percent = value;
                invalidate();
            }
        });
        animatorValue.start();
        this.lastPercent = percent;
    }

    /**
     * 设置渐变颜色起点颜色(percent=0)
     *
     * @param color ColorInt，argb
     */
    public void setStartColor(final int color) {
        if (this.startColor != color) {
            this.startColor = color;
            // delta变化
            refreshDelta();
            // 渐变前部分
            customColors[0] = new Color(color);
            // 前半圆
            startPaint.setColor(new Color(color));
            // 全满时 渐变起点
            fullColors[0] = new Color(color);
            invalidate();
        }
    }

    public int getStartColor() {
        return this.startColor;
    }

    /**
     * 设置渐变颜色终点颜色(percent=1)
     *
     * @param color ColorInt，argb
     */
    public void setEndColor(final int color) {
        if (this.endColor != color) {
            this.endColor = color;
            // delta变化
            refreshDelta();
            // 渐变后部分 动态计算#draw
            // 后半圆 需要动态计算#draw，在某些情况下没有
            // 全满时 渐变结束
            fullColors[1] = new Color(color);
            invalidate();
        }
    }

    public int getEndColor() {
        return this.endColor;
    }

    /**
     * 设置未填充部分的描边的颜色
     *
     * @param color ColorInt，argb
     */
    public void setDefaultColor(final int color) {
        if (this.defaultColor != color) {
            this.defaultColor = color;
            // 渐变后半部分
            customColors[2] = new Color(color);
            customColors[3] = new Color(color);
            // percent = 0
            emptyColors[0] = new Color(color);
            emptyColors[1] = new Color(color);
            invalidate();
        }
    }

    public int getDefaultColor() {
        return this.defaultColor;
    }

    /**
     * 设置描边宽度
     *
     * @param width px
     */
    public void setStrokeWidth(final int width) {
        if (this.strokeWidth != width) {
            this.strokeWidth = width;
            // 画描边的描边变化
            paint.setStrokeWidth(width);
            // 会影响measure
            invalidate();
        }
    }

    public int getStrokeWidth() {
        return this.strokeWidth;
    }

    /**
     * 设置结尾的圆弧是否覆盖开始的圆弧
     *
     * @param footOverHead Boolean
     */
    public void setFootOverHead(boolean footOverHead) {
        if (this.isFootOverHead != footOverHead) {
            this.isFootOverHead = footOverHead;
            invalidate();
        }
    }

    public boolean isFootOverHead() {
        return isFootOverHead;
    }

    private int deltaR;
    private int deltaB;
    private int deltaG;
    private int startR;
    private int startB;
    private int startG;

    private void calculatePercentEndColor(final float percent) {
        percentEndColor = ((int) (deltaR * percent + startR) << 16) +
                ((int) (deltaG * percent + startG) << 8) +
                ((int) (deltaB * percent + startB)) + 0xFF000000;
    }

    @Override
    public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {
        this.rectF.left = EstimateSpec.getSize(widthEstimateConfig) / 2 - strokeWidth / 2;
        this.rectF.top = 0;
        this.rectF.right = EstimateSpec.getSize(widthEstimateConfig) / 2 + strokeWidth / 2;
        this.rectF.bottom = strokeWidth;
        return false;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        final int restore = canvas.save();
        final int width = getWidth();
        final int height = getHeight();

        final int cx = width / 2;
        final int cy = height / 2;
        final int radius = width / 2 - strokeWidth / 2;

        float drawPercent = percent;


        // 画渐变圆
        canvas.save();
        canvas.rotate(-90, cx, cy);
        Color[] colors;
        float[] positions;
        if (drawPercent < 1 && drawPercent > 0) {
            calculatePercentEndColor(drawPercent);
            customColors[1] = new Color(percentEndColor);
            colors = customColors;
            customPositions[1] = drawPercent;
            customPositions[2] = drawPercent;
            positions = customPositions;
        } else if (drawPercent == 1) {
            percentEndColor = endColor;
            colors = fullColors;
            positions = extremePositions;
        } else {
            // <= 0 || > 1?
            colors = emptyColors;
            positions = extremePositions;
        }
        final SweepShader sweepGradient = new SweepShader(cx, cy, colors, positions);
        paint.setShader(sweepGradient, Paint.ShaderType.SWEEP_SHADER);
        canvas.drawCircle(cx, cy, radius, paint);
        canvas.restore();

        if (drawPercent > 0) {
            // 绘制结束的半圆
            if (drawPercent < 1 || (isFootOverHead && drawPercent == 1)) {
                canvas.save();
                endPaint.setColor(new Color(percentEndColor));
                canvas.rotate((int) Math.floor(360.0f * drawPercent) - 1, cx, cy);
                canvas.drawArc(rectF, new Arc(-90f, 180f, true), endPaint);
                canvas.restore();
            }
            if (!isFootOverHead || drawPercent < 1) {
                canvas.save();
                // 绘制开始的半圆
                canvas.drawArc(rectF, new Arc(90f, 180f, true), startPaint);
                canvas.restore();
            }
        }

        canvas.restoreToCount(restore);

    }

    private float vpToPx(Context context, float vp) {
        return context.getResourceManager().getDeviceCapability().screenDensity / 160 * vp;
    }

    private float percentIn01(float percent) {
        percent = Math.min(1, percent);
        percent = Math.max(0, percent);
        return percent;
    }

}
