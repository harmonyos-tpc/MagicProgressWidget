/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.liulishuo.magicprogresswidget;

/**
 * 设置关于进度的接口
 */
public interface ISmoothTarget {

    /**
     * 获取当前进度
     * @return 当前进度
     */
    float getPercent();

    /**
     * 设置第一次的进度
     *
     * @param percent 第一次的进度(from = 0.0, to = 1.0)
     */
    void setPercent(float percent);

    /**
     * 设置第一次的进度
     *
     * @param percent        第一次的进度(from = 0.0, to = 1.0)
     * @param durationMillis 动画执行时间
     */
    void setPercent(float percent, long durationMillis);

    /**
     * 设置当前进度
     *
     * @param percent 当前进度
     */
    void setSmoothPercent(float percent);

    /**
     * 设置当前进度
     *
     * @param percent        当前进度
     * @param durationMillis 动画执行时间
     */
    void setSmoothPercent(float percent, long durationMillis);

}
