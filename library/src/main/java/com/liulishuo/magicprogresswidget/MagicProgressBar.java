/*
 * Copyright (c) 2015 LingoChamp Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.liulishuo.magicprogresswidget;

import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

/**
 * Created by Jacksgong on 12/8/15.
 * <p/>
 * 轻量的ProgressBar
 */
public class MagicProgressBar extends Component implements ISmoothTarget, Component.DrawTask, Component.EstimateSizeListener {

    private static final String MPB_PERCENT = "mpb_percent";
    private static final String MPB_FILL_COLOR = "mpb_fill_color";
    private static final String MPB_BACKGROUND_COLOR = "mpb_background_color";
    private static final String MPB_FLAT = "mpb_flat";

    private int fillColor;
    private int backgroundColor;
    private Paint fillPaint;
    private Paint backgroundPaint;
    private float percent;
    private float lastPercent;
    private boolean isFlat;
    private MyValueAnimator animatorValue;
    private long maxTime = 4000;

    public MagicProgressBar(Context context) {
        super(context);
        init(context, null);
    }

    public MagicProgressBar(Context context, AttrSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public MagicProgressBar(Context context, AttrSet attrs, String styleName) {
        super(context, attrs, styleName);
        init(context, attrs);
    }

    public MagicProgressBar(Context context, AttrSet attrs, int resId) {
        super(context, attrs, resId);
        init(context, attrs);
    }

    private void init(final Context context, AttrSet attrs) {
        if (context == null || attrs == null) {
            return;
        }

        AttrUtil attrUtil = new AttrUtil(attrs);
        percent = attrUtil.getFloatValue(MPB_PERCENT, 0);
        fillColor = attrUtil.getColorValue(MPB_FILL_COLOR, 0);
        backgroundColor = attrUtil.getColorValue(MPB_BACKGROUND_COLOR, 0);
        isFlat = attrUtil.getBooleanValue(MPB_FLAT, false);

        fillPaint = new Paint();
        fillPaint.setColor(new Color(fillColor));
        fillPaint.setAntiAlias(true);

        backgroundPaint = new Paint();
        backgroundPaint.setColor(new Color(backgroundColor));
        backgroundPaint.setAntiAlias(true);

        addDrawTask(this);
        setEstimateSizeListener(this);
    }

    /**
     * 设置填充进度的颜色
     *
     * @param fillColor 填充进度的颜色，argb
     */
    public void setFillColor(final int fillColor) {
        if (this.fillColor != fillColor) {
            this.fillColor = fillColor;
            this.fillPaint.setColor(new Color(fillColor));
            invalidate();
        }

    }

    /**
     * 进度背景的颜色
     *
     * @param backgroundColor 进度背景的颜色,argb
     */
    public void setBackgroundColor(final int backgroundColor) {
        if (this.backgroundColor != backgroundColor) {
            this.backgroundColor = backgroundColor;
            this.backgroundPaint.setColor(new Color(backgroundColor));
            invalidate();
        }
    }

    public int getFillColor() {
        return this.fillColor;
    }

    public int getBackgroundColor() {
        return this.backgroundColor;
    }

    @Override
    public float getPercent() {
        return this.percent;
    }

    @Override
    public void setPercent(float percent) {
        percent = percentIn01(percent);
        anim(0, percent, (long) (percent * maxTime));
    }

    @Override
    public void setPercent(float percent, long durationMillis) {
        percent = percentIn01(percent);
        anim(0, percent, durationMillis);
    }

    @Override
    public void setSmoothPercent(float percent) {
        percent = percentIn01(percent);
        anim(lastPercent, percent, (long) ((percent - lastPercent) * maxTime));
    }

    @Override
    public void setSmoothPercent(float percent, long durationMillis) {
        percent = percentIn01(percent);
        anim(lastPercent, percent, durationMillis);
    }

    private void anim(float lastPercent, float percent, long duration) {
        if (animatorValue != null) {
            if (animatorValue.isRunning()) {
                animatorValue.end();
            }
            animatorValue.release();
        }
        animatorValue = MyValueAnimator.ofFloat(lastPercent, percent);
        animatorValue.setDuration(duration);
        animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float value) {
                MagicProgressBar.this.percent = value;
                invalidate();
            }
        });
        animatorValue.start();
        this.lastPercent = percent;
    }

    /**
     * 设置填充的进度条右侧是否是平面(不是平面就是圆弧)
     *
     * @param flat Whether the right side of progress is round or flat
     */
    public void setFlat(final boolean flat) {
        if (this.isFlat != flat) {
            this.isFlat = flat;
            invalidate();
        }
    }

    private final RectFloat rectF = new RectFloat();
    private int width;
    private int height;
    private float radius;

    @Override
    public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {
        width = EstimateSpec.getSize(widthEstimateConfig) - getPaddingLeft() - getPaddingRight();
        height = EstimateSpec.getSize(heightEstimateConfig) - getPaddingTop() - getPaddingBottom();
        radius = height / 2.0f;
        return false;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        float drawPercent = percent;
        float fillWidth = drawPercent * width;
        rectF.left = 0;
        rectF.top = 0;
        rectF.right = width;
        rectF.bottom = height;
        canvas.save();
        // draw background
        if (backgroundColor != 0) {
            canvas.drawRoundRect(rectF, radius, radius, backgroundPaint);
        }
        // draw fill
        try {
            if (fillColor != 0 && fillWidth > 0) {
                if (drawPercent == 1) {
                    rectF.right = fillWidth;
                    canvas.drawRoundRect(rectF, radius, radius, fillPaint);
                    return;
                }
                if (isFlat) {
                    // draw left semicircle
                    canvas.save();
                    rectF.right = fillWidth > radius ? radius : fillWidth;
                    canvas.clipRect(rectF);
                    rectF.right = radius * 2;
                    canvas.drawRoundRect(rectF, radius, radius, fillPaint);
                    canvas.restore();
                    if (fillWidth <= radius) {
                        return;
                    }
                    float leftAreaWidth = width - radius;
                    // draw center
                    float centerX = fillWidth > leftAreaWidth ? leftAreaWidth : fillWidth;
                    rectF.left = radius;
                    rectF.right = centerX;
                    canvas.drawRect(rectF, fillPaint);
                    if (fillWidth <= leftAreaWidth) {
                        return;
                    }
                    // draw right semicircle
                    rectF.left = leftAreaWidth - radius;
                    rectF.right = fillWidth;
                    canvas.clipRect(rectF);
                    rectF.right = width;
                    canvas.drawArc(rectF, new Arc(-90, 180, true), fillPaint);
                } else {
                    if (fillWidth <= radius * 2) {
                        rectF.right = fillWidth;
                        canvas.clipRect(rectF);
                        rectF.right = radius * 2;
                        canvas.drawRoundRect(rectF, radius, radius, fillPaint);
                    } else {
                        rectF.right = fillWidth;
                        canvas.drawRoundRect(rectF, radius, radius, fillPaint);
                    }
                }
            }
        } finally {
            canvas.restore();
        }
    }

    private float percentIn01(float percent) {
        percent = Math.min(1, percent);
        percent = Math.max(0, percent);
        return percent;
    }

}
