# MagicProgressWidget

> 渐变的圆形进度条与轻量横向进度条

## I. 最终效果

<img src="https://gitee.com/HarmonyOS-tpc/MagicProgressWidget/raw/master/art/MagicProgressWidget.gif" width="75%"/>

## II. 如何使用
```
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:MagicProgressWidget:1.0.2'
```

> 建议参考示例entry

```
<com.liulishuo.magicprogresswidget.MagicProgressCircle
    ohos:id="$+id:demo_mpc"
    ohos:height="50vp"
    ohos:width="50vp"
    ohos:mpc_percent="0.8"
    ohos:mpc_start_color="$color:mpc_start_color"
    ohos:mpc_end_color="$color:mpc_end_color"
    ohos:mpc_stroke_width="18vp"
    ohos:mpc_default_color="$color:mpc_default_color"/>

<com.liulishuo.magicprogresswidget.MagicProgressBar
    ohos:id="$+id:demo_mpb"
    ohos:height="8vp"
    ohos:width="match_parent"
    ohos:mpb_fill_color="$color:mpb_color"
    ohos:mpb_background_color="$color:mpb_default_color"
    ohos:mpb_flat="true"/>
```

#### 1. Magic Progress Circle

- 支持平滑过渡: `setSmoothPercent(percent:float):void`
- 支持指定时间的平滑过渡: `setSmoothPercent(percent:float, durationMillis:long):void`

| 参数                | 含义                     | 默认值     |
| :---               | :---                     | :---      |
| mpc_percent        | 填充的百分比[0, 1]         | 0         |
| mpc_stroke_width   | 描边宽度                  | 18vp      |
| mpc_start_color    | 渐变颜色起点颜色(percent=0) | #FF00FFED |
| mpc_end_color      | 渐变颜色终点颜色(percent=1) | #FFED00FF |
| mpc_default_color  | 未填充部分的描边的颜色       | #1AFFFFFF |
| mpc_foot_over_head | 结尾的圆弧是否覆盖开始的圆弧  | false     |

#### 2. Magic Progress Bar

> 相比系统的ProgressBar更加轻量，如果你的ProgressBar要求不是很复杂，推荐使用

- 支持平滑过渡: `setSmoothPercent(percent:float):void`
- 支持指定时间的平滑过渡: `setSmoothPercent(percent:float, durationMillis:long):void`

| 参数                  | 含义                                 | 默认值 |
| :---                 | :---                                 | :---  |
| mpb_percent          | 填充的百分比[0, 1]                     | 0     |
| mpb_fill_color       | 填充进度的颜色                          | 0     |
| mpb_background_color | 进度背景的颜色                          | 0     |
| mpb_flat             | 填充的进度条右侧是否是平面(不是平面就是圆弧) | false |

## III. 示例entry运行要求
通过DevEco studio,并下载SDK
将项目中的build.gradle文件中dependencies→classpath版本改为对应的版本（即你的IDE新建项目中所用的版本）

## IV. LICENSE

```
Copyright (c) 2015 LingoChamp Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```

