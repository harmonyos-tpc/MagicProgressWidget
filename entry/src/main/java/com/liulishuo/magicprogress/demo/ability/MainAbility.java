package com.liulishuo.magicprogress.demo.ability;

import com.liulishuo.magicprogress.demo.ResourceTable;
import com.liulishuo.magicprogress.demo.widget.AnimTextView;
import com.liulishuo.magicprogresswidget.ISmoothTarget;
import com.liulishuo.magicprogresswidget.MagicProgressBar;
import com.liulishuo.magicprogresswidget.MagicProgressCircle;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 进度条演示页面
 */
public class MainAbility extends Ability implements Component.ClickedListener {

    private MagicProgressCircle demoMpc;
    private AnimTextView demoTv;
    private MagicProgressBar demo1Mpb;
    private MagicProgressBar demo2Mpb;
    private MagicProgressBar demo3Mpb;
    private MagicProgressBar demo4Mpb;
    private SecureRandom random;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);
        try {
            random = SecureRandom.getInstance("SHA1PRNG");
        } catch (NoSuchAlgorithmException e) {
            Logger.getLogger(MainAbility.class.getName()).log(Level.SEVERE, e.getMessage());
        }
        assignViews();
        suiji();
    }

    private void assignViews() {
        demoMpc = (MagicProgressCircle) findComponentById(ResourceTable.Id_demo_mpc);
        demoTv = (AnimTextView) findComponentById(ResourceTable.Id_demo_tv);
        demo1Mpb = (MagicProgressBar) findComponentById(ResourceTable.Id_demo_1_mpb);
        demo2Mpb = (MagicProgressBar) findComponentById(ResourceTable.Id_demo_2_mpb);
        demo3Mpb = (MagicProgressBar) findComponentById(ResourceTable.Id_demo_3_mpb);
        demo4Mpb = (MagicProgressBar) findComponentById(ResourceTable.Id_demo_4_mpb);
        findComponentById(ResourceTable.Id_re_random_percent).setClickedListener(this);
        findComponentById(ResourceTable.Id_increase_progress_smoothly).setClickedListener(this);
    }

    private void suiji() {
        final int ceil = 26;
        final long duration = 600;
        final int progress = random.nextInt(ceil);
        demoMpc.setPercent(progress / 100f, duration);
        demoTv.setPercent(progress / 100f, duration);
        demo1Mpb.setPercent(random.nextInt(ceil) / 100f, duration);
        demo2Mpb.setPercent(random.nextInt(ceil) / 100f, duration);
        demo3Mpb.setPercent(random.nextInt(ceil) / 100f, duration);
        demo4Mpb.setPercent(random.nextInt(ceil) / 100f, duration);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_re_random_percent:
                suiji();
                break;
            case ResourceTable.Id_increase_progress_smoothly:
                float mpcPercent = getIncreasedPercent(demoMpc);
                demoMpc.setSmoothPercent(mpcPercent);
                demoTv.setSmoothPercent(mpcPercent);
                // Just for demo smoothly process to the target percent in 3000ms duration.
                demo1Mpb.setSmoothPercent(getIncreasedPercent(demo1Mpb), 3000);
                demo2Mpb.setSmoothPercent(getIncreasedPercent(demo2Mpb));
                demo3Mpb.setSmoothPercent(getIncreasedPercent(demo3Mpb));
                demo4Mpb.setSmoothPercent(getIncreasedPercent(demo4Mpb));
                break;
            default:
                break;
        }
    }

    private float getIncreasedPercent(ISmoothTarget target) {
        float increasedPercent = target.getPercent() + 0.1f;
        return Math.min(1, increasedPercent);
    }

}
